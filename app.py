#!/usr/bin/env python

import json

from tempfile import NamedTemporaryFile

from tornado.web import Application, RequestHandler
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

# logging
import logging
import sys

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

from relatorio import Report

class MainHandler(RequestHandler):
    def return_html_error(self, msg="Error"):
        self.clear()
        self.set_status(400)
        self.finish(f"<html><body>{msg}</body></html>")

    def post(self):
        if 'variables' not in self.request.arguments:
            error_msg = 'Error : field variables is missing'
            logging.info(error_msg)
            self.raisreturn_html_errore_error(error_msg)

        if 'template' not in self.request.files:
            error_msg = 'Error : field template is missing'
            logging.info(error_msg)
            self.return_html_error(error_msg)

        variables_str = self.get_argument('variables')
        variables = json.loads(variables_str)

        template_files = self.request.files['template']
        template_file = template_files[0]
        template_filename, template_content_type = template_file["filename"], template_file["content_type"]
        template_body = template_file["body"]

        logging.info(
            f'POST "{template_filename}" "{template_content_type}" {len(template_body)} bytes'
        )

        with NamedTemporaryFile() as tmp_file:
            tmp_file.write(template_body)

            ODT_MIME = 'application/vnd.oasis.opendocument.text'
            report = Report(tmp_file.name, ODT_MIME)
            try:
                content = report(v=variables).render().getvalue()
                self.write(content)
            except AssertionError as e:
                ret = { 'error': True, 'message': f"{e}" }
                self.add_header('Content-Type', 'application/x-json')
                self.write(json.dumps(ret))
                self.set_status(400)

            self.finish()

def main():
    app = Application([
        (r"/", MainHandler),
        ], debug=True)
    http_server = HTTPServer(app)
    http_server.listen(8888)
    IOLoop.current().start()


if __name__ == "__main__":
    main()
